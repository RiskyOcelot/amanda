package com.riskyocelot.amanda;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;

public class Application extends ApplicationAdapter {
	private PerspectiveCamera camera;
	private Environment environment;
	private CameraInputController cameraController;

	private Surface surface;
	private Menu menu;

	@Override
	public void create () {

		camera = new PerspectiveCamera(60, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.position.set(10f, 10f, 10f);
		camera.lookAt(0,0,0);
		camera.near = 1f;
		camera.far = 300f;
		camera.update();

		environment = new Environment();
		environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1.0f));
		environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1.0f, -0.8f, -0.2f));

		cameraController = new CameraInputController(camera);
		Gdx.input.setInputProcessor(cameraController);

		surface = new Surface(Functions.ACKLEY);
		menu = new Menu();
		menu.create(this.surface);
	}

	@Override
	public void render () {
		cameraController.update();

		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		surface.renderSequence(environment, camera);
		menu.render();
		if(Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE))
		{
			if(menu.isActive)
			{
				Gdx.input.setInputProcessor(cameraController);
			}
			else
			{
				Gdx.input.setInputProcessor(menu.getStage());
			}
			menu.isActive = !menu.isActive;
		}
	}

	@Override
	public void resize(int width, int height) {
		menu.refresh();
	}

	@Override
	public void dispose () {
		surface.disposeSequence();
	}
}
