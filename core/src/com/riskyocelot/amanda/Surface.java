package com.riskyocelot.amanda;

import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;

/**
 * @author RiskyOcelot
 * @since 0.1_pre
 */
public class Surface {
    private Model model;
    private ModelInstance modelInstance;
    private ModelBatch modelBatch;
    private double[][] vertexTab;
    private Mesh mesh;
    private Functions function;

    public Surface(Functions function) {
        modelBatch = new ModelBatch();
        vertexTab = new double[41][41];
        this.function = function;
        create();
    }
    private void create()
    {
        ModelBuilder modelBuilder = new ModelBuilder();

        double ymin = 1000;
        double ymax = -1000;

        for(int x = -20; x <= 20; x++) {
            for(int z = -20; z <= 20; z++) {
                double temp = this.function.getFunction(x/2.0d, z/2.0d);
                vertexTab[x+20][z+20] = temp;
                if(temp < ymin)
                    ymin = temp;
                if(temp > ymax)
                    ymax = temp;
            }
        }
        double amplitude = Math.abs(ymin) + Math.abs(ymax);
        float colorAmp = (float)(1.0/amplitude);

        float[] verts = new float[41*41*20];
        int i = 0;
        for(int x = -20; x <= 20; x++) {
            for(int z = -20; z <= 20; z++) {

                verts[i++] = x;
                verts[i++] = (float)vertexTab[x+20][z+20];
                verts[i++] = z;
                verts[i++] = (float)Math.random();
                verts[i++] = (float)Math.random();
                verts[i++] = (float)Math.random();

                verts[i++] = (float)(1 - colorAmp*vertexTab[x+20][z+20]);
                verts[i++] = 0;
                verts[i++] = (float)(colorAmp*vertexTab[x+20][z+20]);
                verts[i++] = 0;

                verts[i++] = x;
                verts[i++] = (float)vertexTab[x+20][z+20];
                verts[i++] = z;
                verts[i++] = (float)Math.random();
                verts[i++] = -(float)Math.random();
                verts[i++] = (float)Math.random();

                verts[i++] = (float)(1 - colorAmp*vertexTab[x+20][z+20]);
                verts[i++] = 0;
                verts[i++] = (float)(colorAmp*vertexTab[x+20][z+20]);
                verts[i++] = 0;
            }
        }

        short[] indices = new short[40*40*4*3];
        i = 0;
        for(short x = -20; x < 20; x++) {
            for(short z = -20; z < 20; z++) {

                indices[i++] = (short) (82 *(x+20) + 2*(z+20));
                indices[i++] = (short) (82 *(x+20) + 2*(z+21));
                indices[i++] = (short) (82 *(x+21) + 2*(z+20));

                indices[i++] = (short) (82 *(x+21) + 2*(z+20)+1);
                indices[i++] = (short) (82 *(x+20) + 2*(z+21)+1);
                indices[i++] = (short) (82 *(x+20) + 2*(z+20)+1);

                indices[i++] = (short) (82 *(x+21) + 2*(z+20));
                indices[i++] = (short) (82 *(x+20) + 2*(z+21));
                indices[i++] = (short) (82 *(x+21) + 2*(z+21));

                indices[i++] = (short) (82 *(x+21) + 2*(z+21)+1);
                indices[i++] = (short) (82 *(x+20) + 2*(z+21)+1);
                indices[i++] = (short) (82 *(x+21) + 2*(z+20)+1);
            }
        }

        mesh = new Mesh(true, 41*41*20, 40*40*4*3, new VertexAttribute(VertexAttributes.Usage.Position, 3, "a_position"), new VertexAttribute(VertexAttributes.Usage.Normal,3, "a_normal"), new VertexAttribute(VertexAttributes.Usage.ColorUnpacked,4, "a_color"));
        mesh.setVertices(verts);
        mesh.setIndices(indices);
        modelBuilder.begin();
        MeshPartBuilder meshBuilder;
        meshBuilder = modelBuilder.part("part1", GL20.GL_TRIANGLES, new VertexAttributes(new VertexAttribute(VertexAttributes.Usage.Position, 3, "a_position"), new VertexAttribute(VertexAttributes.Usage.Normal,3, "a_normal"), new VertexAttribute(VertexAttributes.Usage.ColorUnpacked,4, "a_color")), new Material());
        meshBuilder.addMesh(mesh);
        model = modelBuilder.end();
        modelInstance = new ModelInstance(model);
    }
    public void setFunction(Functions function) {
        this.function = function;
        create();
    }

    public void renderSequence(Environment environment, Camera camera) {
        modelBatch.begin(camera);
        modelBatch.render(modelInstance, environment);
        modelBatch.end();
    }

    public void disposeSequence() {
        modelBatch.dispose();
        model.dispose();
    }
}
