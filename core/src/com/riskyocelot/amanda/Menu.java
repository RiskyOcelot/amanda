package com.riskyocelot.amanda;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

/**
 * Class created by Light on 11:46 28.05.2016.
 */
public class Menu {
    private Skin skin;
    private Stage stage;
    private Surface surface;

    private TextButton buttonF1;
    private TextButton buttonF2;
    private TextButton buttonRastrig;
    private TextButton buttonSchwefel;
    private TextButton buttonAckley;

    public boolean isActive = false;
    public void create(final Surface surface){
        BitmapFont font = new BitmapFont();
        skin = new Skin();
        skin.add("default", font);

        Pixmap pixmap = new Pixmap(Gdx.graphics.getWidth()/4,Gdx.graphics.getHeight()/10, Pixmap.Format.RGB888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("background",new Texture(pixmap));

        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.up = skin.newDrawable("background", Color.DARK_GRAY);
        textButtonStyle.down = skin.newDrawable("background", Color.DARK_GRAY);
        textButtonStyle.checked = skin.newDrawable("background", Color.DARK_GRAY);
        textButtonStyle.over = skin.newDrawable("background", Color.DARK_GRAY);
        textButtonStyle.font = skin.getFont("default");
        skin.add("default", textButtonStyle);

        buttonF1 = new TextButton("DeJongF1", skin);
        buttonF1.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                surface.setFunction(Functions.DEJONGF1);
                return true;
            }
        });

        buttonF2 = new TextButton("DeJongF2", skin);
        buttonF2.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                surface.setFunction(Functions.DEJONGF2);
                return true;
            }
        });

        buttonSchwefel = new TextButton("Schwefel", skin);
        buttonSchwefel.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                surface.setFunction(Functions.SCHWEFEL);
                return true;
            }
        });

        buttonRastrig = new TextButton("Rastrig", skin);
        buttonRastrig.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                surface.setFunction(Functions.RASTRIG);
                return true;
            }
        });

        buttonAckley = new TextButton("Ackley", skin);
        buttonAckley.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                surface.setFunction(Functions.ACKLEY);
                return true;
            }
        });
    }
    public Stage getStage()
    {
        return stage;
    }
    public void refresh() {
        if(stage != null)
            stage.dispose();
        stage = new Stage();

        buttonF1.setPosition(50 , Gdx.graphics.getHeight() - 100);
        stage.addActor(buttonF1);

        buttonF2.setPosition(50 , Gdx.graphics.getHeight() - 150);
        stage.addActor(buttonF2);

        buttonRastrig.setPosition(50 , Gdx.graphics.getHeight() - 200 );
        stage.addActor(buttonRastrig);

        buttonSchwefel.setPosition(50 , Gdx.graphics.getHeight() - 250);
        stage.addActor(buttonSchwefel);

        buttonAckley.setPosition(50 , Gdx.graphics.getHeight() - 300);
        stage.addActor(buttonAckley);

        if(isActive)
        {
            Gdx.input.setInputProcessor(stage);
        }
    }
    public void render() {
        if(isActive)
        {
            stage.act();
            stage.draw();
        }

    }
}
