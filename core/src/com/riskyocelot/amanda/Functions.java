package com.riskyocelot.amanda;

/**
 * @author RiskyOcelot
 * @since 0.1_pre
 */
public enum Functions {

    DEJONGF1 {
        @Override
        public double getFunction(double x, double z) {
            return deJongF1(x, z);
        }
    },
    DEJONGF2 {
        @Override
        public double getFunction(double x, double z) {
            return deJongF2(x, z);
        }
    },
    SCHWEFEL {
        @Override
        public double getFunction(double x, double z) {
            return schwefel(x, z);
        }
    },
    RASTRIG {
        @Override
        public double getFunction(double x, double z) {
            return rastrig(x, z);

        }
    },
    ACKLEY {
        @Override
        public double getFunction(double x, double z) {
            return ackley(x, z);
        }
    };

    /**
     * Funkcja De Jonga F1
     *
     * @param x
     * @param z
     * @return
     */
    public static double deJongF1(double x, double z) {
        //TODO: -5.12 <= args <= 5.12
        return Math.pow(x, 2) + Math.pow(z, 2);
    }

    /**
     * Funkcja De Jonga F2
     *
     * @param x
     * @param z
     * @return
     */
    public static double deJongF2(double x, double z) {
        //TODO: -5.12 <= args <= 5.12
        return 100 * Math.pow(Math.pow(x, 2) - z, 2) + Math.pow(1 - x, 2);
    }

    /**
     * Funkcja Schwefela
     *
     * @param x
     * @param z
     * @return
     */
    public static double schwefel(double x, double z) {
        //TODO: -500 <= args <= 500
        return -x * Math.sin(Math.sqrt(Math.abs(x))) - z * Math.sin(Math.sqrt(Math.abs(x)));
    }

    /**
     * Funkcja Rastriga
     *
     * @param x
     * @param z
     * @return
     */
    public static double rastrig(double x, double z) {
        //TODO: -5 <= args <= 5
        return 20 + Math.pow(x, 2) + Math.pow(z, 2) - 10 * Math.cos(2 * Math.PI * x) + Math.cos(2 * Math.PI * z);
    }

    /**
     * Funkcja Ackleya
     *
     * @param x
     * @param z
     * @return
     */
    public static double ackley(double x, double z) {
        //TODO: -5 <= args <= 5
        return -20 * Math.exp(-0.2 * Math.sqrt(0.5 * Math.pow(x, 2) + Math.pow(z, 2)))
                - Math.exp(0.5 * Math.cos(2 * Math.PI * x) + Math.cos(2 * Math.PI * z)) + 20 + Math.exp(1d);
    }

    public abstract double getFunction(double x, double z);
}
